# vue-draggable

Vue2 directive for draggable elements.


Usage:
<div v-draggable="{ no_draggable: ['images','description'] }">
	...
</div>